﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindSingle
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] x = { 1,1,2,3,3,4,5,9,9,5,4};
            Dictionary<int, int> d = new Dictionary<int, int>();

            foreach (var n in x)
            {
                
                if (!d.ContainsKey(n))
                {
                    d.Add(n, 0);
                }
                d[n]++;
            }
            foreach (var k in d)
            {
                if(k.Value==1)
                Console.WriteLine(k.Key);
            }
        }
    }
}
